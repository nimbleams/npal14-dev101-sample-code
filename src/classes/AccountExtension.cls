public with sharing class AccountExtension {
    public Account acct { get; set; }

    public List<NU__Order__c> ordersWithBalance { get; set; }

    public List<AggregateResult> pieData
    {
        get {
            if (pieData == null) {
                pieData = QueryPieData();
            }
            return pieData;
        }
        set;
    }

    public AccountExtension(ApexPages.StandardController stc) {
        if (!Test.isRunningTest()) {
            stc.addFields(new List<string> {'NU__Lapsed__c'});
        }

        acct = (Account)stc.getRecord();

        CheckAccount();
    }

    public void CheckAccount() {
        if (acct != null) {
            CheckMembershipStatus();

            CheckOrdersWithBalance();

            if(!ApexPages.hasMessages()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Everything checks out for this account.'));
            }
        }
    }

    public void CheckMembershipStatus() {
        if (acct.NU__Lapsed__c == 'Yes') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Membership lapsed! Consider sending follow up email.'));
        }
    }

    public List<NU__Order__c> QueryOrdersWithBalance() {
    return [SELECT Id, Name, NU__Balance__c, NU__BillTo__c,
            NU__InvoiceGenerated__c, NU__InvoiceDaysOutstanding__c,
                    NU__InvoiceDueDate__c, NU__TotalPayment__c, NU__TransactionDate__c,
                    NU__GrandTotal__c
                FROM NU__Order__c
                WHERE NU__BillTo__c = :acct.Id AND NU__Balance__c > 0];
    }

    public void CheckOrdersWithBalance() {
        ordersWithBalance = QueryOrdersWithBalance();

        if (ordersWithBalance != null && ordersWithBalance.size() > 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'There are orders with outstanding balances.'));
        }
    }

    private List<AggregateResult> QueryPieData() {
        return [SELECT NU__Event__r.NU__Type__c eventType,
                       COUNT(NU__Account__c) amount
                   FROM NU__Registration2__c
                   WHERE (NU__Account__c = :acct.Id)
                   GROUP BY NU__Event__r.NU__Type__c];
    }
}