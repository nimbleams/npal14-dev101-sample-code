@isTest
public class TestAccountExtension {
    public static final string TEST_EVENT_TYPE1 = 'Conference';
    public static final string TEST_EVENT_TYPE2 = 'Webinar';

    public static testMethod void testAccountLoadSuccessMsg() {
        Account acct = new Account(FirstName='Zach', LastName='Field');
        insert acct;

        Test.startTest();
        PageReference pr = Page.CustomAccountLayout;
        Test.setCurrentPageReference(pr);
    ApexPages.StandardController std = new ApexPages.StandardController(acct);
        AccountExtension stdExt = new AccountExtension(std);
        Test.stopTest();

        system.assert(ApexPages.hasMessages(ApexPages.Severity.CONFIRM), 'Expected CONFIRM message, found none.');
    }

    public static testMethod void testAccountMembershipLapsedMsg() {
        Account acct = new Account(FirstName='Bob', LastName='Testman');
        insert acct;

        //use NU data factory to insert expired membership
        NU.DataFactoryMembership.insertDefaultExpiredMembership(acct.Id);

        //we need to query the account we inserted with the NU__Lapsed__c field
        List<Account> accountsToTest = [SELECT Id, NU__Lapsed__c FROM Account WHERE Id = :acct.Id];

        //make sure we retrieved account records
        system.assert(accountsToTest != null && accountsToTest.size() > 0, 'Test accounts not found, check SOQL query.');

        //reset our acct variable to be the same account, just with our NU__Lapsed__c field value
        acct = accountsToTest[0];

        Test.startTest();
        PageReference pr = Page.CustomAccountLayout;
        Test.setCurrentPageReference(pr);
    ApexPages.StandardController std = new ApexPages.StandardController(acct);
        AccountExtension stdExt = new AccountExtension(std);
        Test.stopTest();

        system.assert(ApexPages.hasMessages(ApexPages.Severity.INFO), 'Expected INFO message, found none.');
    }
}